set -e
echo "Building document"
make pdf
mkdir public
mv build/musiktheorie.pdf public
mv build/musiktheorie.log public
cd public/
if ! command -v tree &> /dev/null
then
  echo "No tree utility found, skipping making tree"
else
  tree -H '.' -I "index.html" -D --charset utf-8 -T "musiktheorie" > index.html
fi
