# Musiktheorietheorie 


Notizen zum Kurs Mathematische Musiktheorie von Georg Schröter

CdE MusikAkademie 2022 



Die Notizen sind unvollständig und enthalten vermutlich einige Fehler.



Die neuste Version ist [hier][1] als pdf verfügbar.

[1]: https://jrpie-notes.gitlab.io/cde/musiktheorietheorie/musiktheorie.pdf
